//
//  main.m
//  First
//
//  Created by Nitigron Ruengmontre on 10/2/56 BE.
//  Copyright (c) 2556 Appsynth. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ASAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ASAppDelegate class]));
    }
}
