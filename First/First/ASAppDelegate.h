//
//  ASAppDelegate.h
//  First
//
//  Created by Nitigron Ruengmontre on 10/2/56 BE.
//  Copyright (c) 2556 Appsynth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
